<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.3.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="con-wago_5.0mm">
<description>&lt;b&gt;WAGO picoMAX 5.0 - Serie 2092&lt;br&gt;
Rastermaß/ Pitch 5 mm
&lt;/b&gt;&lt;br&gt;

&lt;UL&gt;
&lt;LI&gt; Leiterquerschnitte bis 2,5 mm²&lt;br&gt;
Conductor size up to 2.5 mm²&lt;/LI&gt;

&lt;br&gt;

&lt;LI&gt; Gerade und abgewinkelte Lötstifte für rechtwinklige und parallele Steckrichtung zur Leiterplatte &lt;br&gt;
Horizontal or vertical PCB mounting via straight or angled solder pins &lt;/LI&gt;

&lt;br&gt;

&lt;LI&gt; Stift- und Federleiste mit Lötstiften&lt;br&gt;
Male and Femail Header with solder pins&lt;/LI&gt;

&lt;br&gt;

&lt;LI&gt; THR-Varianten zur Verarbeitung im Reflow-Lötprozess in der SMT-Fertigung &lt;br&gt;
THR versions for reflow soldering in SMT applications &lt;/LI&gt;

&lt;br&gt;

&lt;LI&gt; Tape-and-Reel-Verpackungen für die automatische Bestückung der Komponenten auf die Leiterplatte &lt;br&gt;
Available in tape-and-reel packaging for automated pick-and-place assembly&lt;/LI&gt;

&lt;br&gt;
&lt;/UL&gt;</description>
<packages>
<package name="P-2092-1424">
<wire x1="1.45" y1="8.2" x2="-5.75" y2="8.2" width="0.01" layer="51"/>
<wire x1="-5.75" y1="6.8" x2="1.45" y2="6.8" width="0.01" layer="51"/>
<wire x1="2.85" y1="7.75" x2="1.45" y2="8.2" width="0.01" layer="51"/>
<wire x1="1.45" y1="6.8" x2="2.85" y2="7.25" width="0.01" layer="51"/>
<wire x1="1.45" y1="-6.8" x2="-5.75" y2="-6.8" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-8.2" x2="1.45" y2="-8.2" width="0.01" layer="51"/>
<wire x1="2.85" y1="-7.25" x2="1.45" y2="-6.8" width="0.01" layer="51"/>
<wire x1="1.45" y1="-8.2" x2="2.85" y2="-7.75" width="0.01" layer="51"/>
<wire x1="1.45" y1="-1.8" x2="-5.75" y2="-1.8" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-3.2" x2="1.45" y2="-3.2" width="0.01" layer="51"/>
<wire x1="2.85" y1="-2.25" x2="1.45" y2="-1.8" width="0.01" layer="51"/>
<wire x1="1.45" y1="-3.2" x2="2.85" y2="-2.75" width="0.01" layer="51"/>
<wire x1="1.45" y1="3.2" x2="-5.75" y2="3.2" width="0.01" layer="51"/>
<wire x1="-5.75" y1="1.8" x2="1.45" y2="1.8" width="0.01" layer="51"/>
<wire x1="2.85" y1="2.75" x2="1.45" y2="3.2" width="0.01" layer="51"/>
<wire x1="1.45" y1="1.8" x2="2.85" y2="2.25" width="0.01" layer="51"/>
<wire x1="8.15" y1="-11.1" x2="-9.75" y2="-11.1" width="0.2" layer="21"/>
<wire x1="7.65" y1="11.1" x2="7.65" y2="10.3" width="0.01" layer="51"/>
<wire x1="7.65" y1="-10.3" x2="7.65" y2="-11.1" width="0.01" layer="51"/>
<wire x1="-1.35" y1="10.3" x2="-1.35" y2="-10.3" width="0.01" layer="51"/>
<wire x1="8.15" y1="10.3" x2="-1.35" y2="10.3" width="0.01" layer="51"/>
<wire x1="9.75" y1="-9.05" x2="8.15" y2="-9.05" width="0.2" layer="21"/>
<wire x1="5.65" y1="-9.05" x2="3.95" y2="-9.05" width="0.01" layer="51"/>
<wire x1="3.95" y1="-9.05" x2="2.55" y2="-9.05" width="0.01" layer="51"/>
<wire x1="2.55" y1="-9.05" x2="-5.75" y2="-9.05" width="0.01" layer="51"/>
<wire x1="8.15" y1="-10.3" x2="-1.35" y2="-10.3" width="0.01" layer="51"/>
<wire x1="8.15" y1="10.2" x2="7.7794" y2="10.2" width="0.01" layer="51"/>
<wire x1="6.0206" y1="10.2" x2="-7.25" y2="10.2" width="0.01" layer="51"/>
<wire x1="8.15" y1="-10.2" x2="7.7794" y2="-10.2" width="0.01" layer="51"/>
<wire x1="6.0206" y1="-10.2" x2="-7.25" y2="-10.2" width="0.01" layer="51"/>
<wire x1="-9.15" y1="-0.5" x2="-9.15" y2="-1.78" width="0.01" layer="51"/>
<wire x1="-9.15" y1="4.5" x2="-9.15" y2="3.22" width="0.01" layer="51"/>
<wire x1="-7.95" y1="10" x2="-8.35" y2="10" width="0.01" layer="51"/>
<wire x1="-8.35" y1="10" x2="-9.15" y2="10" width="0.01" layer="51"/>
<wire x1="-9.15" y1="10" x2="-9.1892" y2="10" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="10" x2="-9.35" y2="10" width="0.01" layer="51"/>
<wire x1="-9.15" y1="10" x2="-9.15" y2="8.22" width="0.01" layer="51"/>
<wire x1="-9.15" y1="6.78" x2="-9.15" y2="5.5" width="0.01" layer="51"/>
<wire x1="-9.35" y1="5.5" x2="-9.1892" y2="5.5" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="5.5" x2="-8.35" y2="5.5" width="0.01" layer="51"/>
<wire x1="-8.35" y1="5.5" x2="-7.95" y2="5.5" width="0.01" layer="51"/>
<wire x1="-9.35" y1="-10" x2="-9.1892" y2="-10" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="-10" x2="-9.15" y2="-10" width="0.01" layer="51"/>
<wire x1="-9.15" y1="-10" x2="-8.35" y2="-10" width="0.01" layer="51"/>
<wire x1="-8.35" y1="-10" x2="-7.95" y2="-10" width="0.01" layer="51"/>
<wire x1="-9.15" y1="-8.22" x2="-9.15" y2="-10" width="0.01" layer="51"/>
<wire x1="-9.15" y1="-5.5" x2="-9.15" y2="-6.78" width="0.01" layer="51"/>
<wire x1="-9.15" y1="-3.22" x2="-9.15" y2="-4.5" width="0.01" layer="51"/>
<wire x1="-7.95" y1="-0.5" x2="-8.35" y2="-0.5" width="0.01" layer="51"/>
<wire x1="-8.35" y1="-0.5" x2="-9.1892" y2="-0.5" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="-0.5" x2="-9.35" y2="-0.5" width="0.01" layer="51"/>
<wire x1="-9.15" y1="1.78" x2="-9.15" y2="0.5" width="0.01" layer="51"/>
<wire x1="-9.35" y1="0.5" x2="-9.1892" y2="0.5" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="0.5" x2="-8.35" y2="0.5" width="0.01" layer="51"/>
<wire x1="-8.35" y1="0.5" x2="-7.95" y2="0.5" width="0.01" layer="51"/>
<wire x1="-7.95" y1="4.5" x2="-8.35" y2="4.5" width="0.01" layer="51"/>
<wire x1="-8.35" y1="4.5" x2="-9.1892" y2="4.5" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="4.5" x2="-9.35" y2="4.5" width="0.01" layer="51"/>
<wire x1="-9.35" y1="-4.5" x2="-9.1892" y2="-4.5" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="-4.5" x2="-8.35" y2="-4.5" width="0.01" layer="51"/>
<wire x1="-8.35" y1="-4.5" x2="-7.95" y2="-4.5" width="0.01" layer="51"/>
<wire x1="-7.95" y1="-5.5" x2="-8.35" y2="-5.5" width="0.01" layer="51"/>
<wire x1="-8.35" y1="-5.5" x2="-9.1892" y2="-5.5" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="-5.5" x2="-9.35" y2="-5.5" width="0.01" layer="51"/>
<wire x1="-6.55" y1="6.535" x2="2.55" y2="6.535" width="0.01" layer="51"/>
<wire x1="-5.75" y1="9.05" x2="2.55" y2="9.05" width="0.01" layer="51"/>
<wire x1="2.55" y1="9.05" x2="3.95" y2="9.05" width="0.01" layer="51"/>
<wire x1="3.95" y1="9.05" x2="5.65" y2="9.05" width="0.01" layer="51"/>
<wire x1="5.65" y1="9.05" x2="8.15" y2="9.05" width="0.01" layer="51"/>
<wire x1="2.55" y1="-9.05" x2="2.55" y2="-6.535" width="0.01" layer="51"/>
<wire x1="2.55" y1="3.465" x2="3.95" y2="3.625" width="0.01" layer="51"/>
<wire x1="2.55" y1="6.535" x2="2.55" y2="9.05" width="0.01" layer="51"/>
<wire x1="3.95" y1="6.375" x2="2.55" y2="6.535" width="0.01" layer="51"/>
<wire x1="5.65" y1="8.25" x2="4.25" y2="8.25" width="0.01" layer="51"/>
<wire x1="4.25" y1="6.75" x2="5.65" y2="6.75" width="0.01" layer="51"/>
<wire x1="5.65" y1="-6.75" x2="4.25" y2="-6.75" width="0.01" layer="51"/>
<wire x1="4.25" y1="-8.25" x2="5.65" y2="-8.25" width="0.01" layer="51"/>
<wire x1="5.65" y1="-1.75" x2="4.25" y2="-1.75" width="0.01" layer="51"/>
<wire x1="4.25" y1="-3.25" x2="5.65" y2="-3.25" width="0.01" layer="51"/>
<wire x1="5.65" y1="3.25" x2="4.25" y2="3.25" width="0.01" layer="51"/>
<wire x1="4.25" y1="1.75" x2="5.65" y2="1.75" width="0.01" layer="51"/>
<wire x1="-7.25" y1="9.45" x2="-5.75" y2="9.45" width="0.01" layer="51"/>
<wire x1="-7.25" y1="10.2" x2="-7.25" y2="6.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="6.55" x2="-7.25" y2="6.3" width="0.01" layer="51"/>
<wire x1="-7.25" y1="6.3" x2="-7.25" y2="6.15" width="0.01" layer="51"/>
<wire x1="-7.25" y1="6.15" x2="-7.25" y2="5.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="5.55" x2="-7.25" y2="4.45" width="0.01" layer="51"/>
<wire x1="-7.25" y1="4.45" x2="-7.25" y2="3.85" width="0.01" layer="51"/>
<wire x1="-7.25" y1="3.85" x2="-7.25" y2="3.7" width="0.01" layer="51"/>
<wire x1="-7.25" y1="3.7" x2="-7.25" y2="3.45" width="0.01" layer="51"/>
<wire x1="-7.25" y1="3.45" x2="-7.25" y2="1.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="1.55" x2="-7.25" y2="1.3" width="0.01" layer="51"/>
<wire x1="-7.25" y1="1.3" x2="-7.25" y2="1.15" width="0.01" layer="51"/>
<wire x1="-7.25" y1="1.15" x2="-7.25" y2="0.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="0.55" x2="-7.25" y2="-0.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-0.55" x2="-7.25" y2="-1.15" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-1.15" x2="-7.25" y2="-1.3" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-1.3" x2="-7.25" y2="-1.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-1.55" x2="-7.25" y2="-3.45" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-3.45" x2="-7.25" y2="-3.7" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-3.7" x2="-7.25" y2="-3.85" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-3.85" x2="-7.25" y2="-4.45" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-4.45" x2="-7.25" y2="-5.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-5.55" x2="-7.25" y2="-6.15" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-6.15" x2="-7.25" y2="-6.3" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-6.3" x2="-7.25" y2="-6.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-6.55" x2="-7.25" y2="-9.45" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-9.45" x2="-7.25" y2="-10.2" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-9.45" x2="-5.75" y2="-6.55" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-6.55" x2="-5.75" y2="-6.31" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-6.31" x2="-5.75" y2="-6.3" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-6.3" x2="-5.75" y2="-5.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-9.45" x2="-5.75" y2="-9.45" width="0.01" layer="51"/>
<wire x1="2.55" y1="-3.465" x2="2.55" y2="-1.535" width="0.01" layer="51"/>
<wire x1="-6.55" y1="3.465" x2="2.55" y2="3.465" width="0.01" layer="51"/>
<wire x1="2.55" y1="1.535" x2="2.55" y2="3.465" width="0.01" layer="51"/>
<wire x1="4.25" y1="1.75" x2="4.25" y2="3.25" width="0.01" layer="51"/>
<wire x1="4.25" y1="-3.25" x2="4.25" y2="-1.75" width="0.01" layer="51"/>
<wire x1="4.25" y1="-8.25" x2="4.25" y2="-6.75" width="0.01" layer="51"/>
<wire x1="4.25" y1="6.75" x2="4.25" y2="8.25" width="0.01" layer="51"/>
<wire x1="3.95" y1="3.625" x2="3.95" y2="1.375" width="0.01" layer="51"/>
<wire x1="-6.55" y1="3.625" x2="3.95" y2="3.625" width="0.01" layer="51"/>
<wire x1="3.95" y1="6.375" x2="-6.55" y2="6.375" width="0.01" layer="51"/>
<wire x1="3.95" y1="9.05" x2="3.95" y2="6.375" width="0.01" layer="51"/>
<wire x1="3.95" y1="-6.375" x2="3.95" y2="-9.05" width="0.01" layer="51"/>
<wire x1="3.95" y1="-1.375" x2="3.95" y2="-3.625" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-6.375" x2="3.95" y2="-6.375" width="0.01" layer="51"/>
<wire x1="2.55" y1="-6.535" x2="3.95" y2="-6.375" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-6.535" x2="2.55" y2="-6.535" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-3.465" x2="2.55" y2="-3.465" width="0.01" layer="51"/>
<wire x1="3.95" y1="-3.625" x2="2.55" y2="-3.465" width="0.01" layer="51"/>
<wire x1="3.95" y1="-3.625" x2="-6.55" y2="-3.625" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-1.375" x2="3.95" y2="-1.375" width="0.01" layer="51"/>
<wire x1="2.55" y1="-1.535" x2="3.95" y2="-1.375" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-1.535" x2="2.55" y2="-1.535" width="0.01" layer="51"/>
<wire x1="-6.55" y1="1.535" x2="2.55" y2="1.535" width="0.01" layer="51"/>
<wire x1="3.95" y1="1.375" x2="2.55" y2="1.535" width="0.01" layer="51"/>
<wire x1="3.95" y1="1.375" x2="-6.55" y2="1.375" width="0.01" layer="51"/>
<wire x1="-6.55" y1="3.85" x2="-6.55" y2="3.69" width="0.01" layer="51"/>
<wire x1="-6.55" y1="3.69" x2="-6.55" y2="3.465" width="0.01" layer="51"/>
<wire x1="-6.55" y1="6.535" x2="-6.55" y2="6.31" width="0.01" layer="51"/>
<wire x1="-6.55" y1="6.31" x2="-6.55" y2="6.15" width="0.01" layer="51"/>
<wire x1="-5.75" y1="5.55" x2="-5.75" y2="6.3" width="0.01" layer="51"/>
<wire x1="-5.75" y1="6.3" x2="-5.75" y2="6.31" width="0.01" layer="51"/>
<wire x1="-5.75" y1="6.31" x2="-5.75" y2="6.55" width="0.01" layer="51"/>
<wire x1="-5.75" y1="6.55" x2="-5.75" y2="9.45" width="0.01" layer="51"/>
<wire x1="-7.25" y1="5.55" x2="-6.55" y2="5.55" width="0.01" layer="51"/>
<wire x1="-6.55" y1="5.55" x2="-5.75" y2="5.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="4.45" x2="-6.55" y2="4.45" width="0.01" layer="51"/>
<wire x1="-6.55" y1="4.45" x2="-5.75" y2="4.45" width="0.01" layer="51"/>
<wire x1="-7.25" y1="3.85" x2="-6.55" y2="3.85" width="0.01" layer="51"/>
<wire x1="-6.55" y1="3.69" x2="-5.75" y2="3.69" width="0.01" layer="51"/>
<wire x1="-7.25" y1="3.45" x2="-5.75" y2="3.45" width="0.01" layer="51"/>
<wire x1="-5.75" y1="6.31" x2="-6.55" y2="6.31" width="0.01" layer="51"/>
<wire x1="-7.25" y1="6.15" x2="-6.55" y2="6.15" width="0.01" layer="51"/>
<wire x1="-7.25" y1="6.55" x2="-5.75" y2="6.55" width="0.01" layer="51"/>
<wire x1="-6.55" y1="4.45" x2="-6.55" y2="5.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="6.3" x2="-5.75" y2="6.3" width="0.01" layer="51"/>
<wire x1="-7.25" y1="3.7" x2="-5.75" y2="3.7" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-6.15" x2="-6.55" y2="-6.31" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-6.31" x2="-6.55" y2="-6.535" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-3.465" x2="-6.55" y2="-3.69" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-3.69" x2="-6.55" y2="-3.85" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-4.45" x2="-5.75" y2="-3.7" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-3.7" x2="-5.75" y2="-3.69" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-3.69" x2="-5.75" y2="-3.45" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-3.45" x2="-5.75" y2="-1.55" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-1.55" x2="-5.75" y2="-1.31" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-1.31" x2="-5.75" y2="-1.3" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-1.3" x2="-5.75" y2="-0.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-4.45" x2="-6.55" y2="-4.45" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-4.45" x2="-5.75" y2="-4.45" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-5.55" x2="-6.55" y2="-5.55" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-5.55" x2="-5.75" y2="-5.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-6.15" x2="-6.55" y2="-6.15" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-6.31" x2="-5.75" y2="-6.31" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-6.55" x2="-5.75" y2="-6.55" width="0.01" layer="51"/>
<wire x1="-5.75" y1="-3.69" x2="-6.55" y2="-3.69" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-3.85" x2="-6.55" y2="-3.85" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-3.45" x2="-5.75" y2="-3.45" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-5.55" x2="-6.55" y2="-4.45" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-3.7" x2="-5.75" y2="-3.7" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-6.3" x2="-5.75" y2="-6.3" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-1.15" x2="-6.55" y2="-1.31" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-1.31" x2="-6.55" y2="-1.535" width="0.01" layer="51"/>
<wire x1="-6.55" y1="1.535" x2="-6.55" y2="1.31" width="0.01" layer="51"/>
<wire x1="-6.55" y1="1.31" x2="-6.55" y2="1.15" width="0.01" layer="51"/>
<wire x1="-5.75" y1="0.55" x2="-5.75" y2="1.3" width="0.01" layer="51"/>
<wire x1="-5.75" y1="1.3" x2="-5.75" y2="1.31" width="0.01" layer="51"/>
<wire x1="-5.75" y1="1.31" x2="-5.75" y2="1.55" width="0.01" layer="51"/>
<wire x1="-5.75" y1="1.55" x2="-5.75" y2="4.45" width="0.01" layer="51"/>
<wire x1="-7.25" y1="0.55" x2="-6.55" y2="0.55" width="0.01" layer="51"/>
<wire x1="-6.55" y1="0.55" x2="-5.75" y2="0.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-0.55" x2="-6.55" y2="-0.55" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-0.55" x2="-5.75" y2="-0.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-1.15" x2="-6.55" y2="-1.15" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-1.31" x2="-5.75" y2="-1.31" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-1.55" x2="-5.75" y2="-1.55" width="0.01" layer="51"/>
<wire x1="-5.75" y1="1.31" x2="-6.55" y2="1.31" width="0.01" layer="51"/>
<wire x1="-7.25" y1="1.15" x2="-6.55" y2="1.15" width="0.01" layer="51"/>
<wire x1="-7.25" y1="1.55" x2="-5.75" y2="1.55" width="0.01" layer="51"/>
<wire x1="-6.55" y1="-0.55" x2="-6.55" y2="0.55" width="0.01" layer="51"/>
<wire x1="-7.25" y1="1.3" x2="-5.75" y2="1.3" width="0.01" layer="51"/>
<wire x1="-7.25" y1="-1.3" x2="-5.75" y2="-1.3" width="0.01" layer="51"/>
<wire x1="1.45" y1="6.8" x2="1.45" y2="8.2" width="0.01" layer="51"/>
<wire x1="2.85" y1="7.25" x2="2.85" y2="7.75" width="0.01" layer="51"/>
<wire x1="1.45" y1="-8.2" x2="1.45" y2="-6.8" width="0.01" layer="51"/>
<wire x1="2.85" y1="-7.75" x2="2.85" y2="-7.25" width="0.01" layer="51"/>
<wire x1="1.45" y1="-3.2" x2="1.45" y2="-1.8" width="0.01" layer="51"/>
<wire x1="2.85" y1="-2.75" x2="2.85" y2="-2.25" width="0.01" layer="51"/>
<wire x1="1.45" y1="1.8" x2="1.45" y2="3.2" width="0.01" layer="51"/>
<wire x1="2.85" y1="2.25" x2="2.85" y2="2.75" width="0.01" layer="51"/>
<wire x1="-9.285" y1="7.5" x2="-7.815" y2="7.5" width="0.01" layer="51"/>
<wire x1="-8.55" y1="6.765" x2="-8.55" y2="8.235" width="0.01" layer="51"/>
<wire x1="-9.285" y1="-7.5" x2="-7.815" y2="-7.5" width="0.01" layer="51"/>
<wire x1="-8.55" y1="-8.235" x2="-8.55" y2="-6.765" width="0.01" layer="51"/>
<wire x1="-9.285" y1="-2.5" x2="-7.815" y2="-2.5" width="0.01" layer="51"/>
<wire x1="-8.55" y1="-3.235" x2="-8.55" y2="-1.765" width="0.01" layer="51"/>
<wire x1="-9.285" y1="2.5" x2="-7.815" y2="2.5" width="0.01" layer="51"/>
<wire x1="-8.55" y1="1.765" x2="-8.55" y2="3.235" width="0.01" layer="51"/>
<wire x1="-6.18" y1="7.5" x2="3.28" y2="7.5" width="0.01" layer="51"/>
<wire x1="-6.18" y1="2.5" x2="3.28" y2="2.5" width="0.01" layer="51"/>
<wire x1="-6.18" y1="-2.5" x2="3.28" y2="-2.5" width="0.01" layer="51"/>
<wire x1="-6.18" y1="-7.5" x2="3.28" y2="-7.5" width="0.01" layer="51"/>
<wire x1="6.0206" y1="-10.2" x2="6.0206" y2="-11.1" width="0.01" layer="51"/>
<wire x1="6.0206" y1="11.1" x2="6.0206" y2="10.2" width="0.01" layer="51"/>
<wire x1="7.7794" y1="-10.2" x2="7.7794" y2="-11.1" width="0.01" layer="51"/>
<wire x1="7.7794" y1="11.1" x2="7.7794" y2="10.2" width="0.01" layer="51"/>
<wire x1="-9.75" y1="11.1" x2="8.15" y2="11.1" width="0.2" layer="21"/>
<wire x1="8.15" y1="-9.05" x2="8.15" y2="-11.1" width="0.2" layer="21"/>
<wire x1="8.15" y1="11.1" x2="8.15" y2="9.05" width="0.2" layer="21"/>
<wire x1="6.0206" y1="10.2" x2="7.7794" y2="10.2" width="0.01" layer="51"/>
<wire x1="7.7794" y1="-10.2" x2="6.0206" y2="-10.2" width="0.01" layer="51"/>
<wire x1="-9.75" y1="5.7399" x2="-7.95" y2="5.7399" width="0.01" layer="51"/>
<wire x1="-9.75" y1="5.9253" x2="-7.95" y2="5.9253" width="0.01" layer="51"/>
<wire x1="-9.75" y1="6.3495" x2="-7.95" y2="6.3495" width="0.01" layer="51"/>
<wire x1="-9.75" y1="6.5349" x2="-7.95" y2="6.5349" width="0.01" layer="51"/>
<wire x1="-9.75" y1="8.4651" x2="-7.95" y2="8.4651" width="0.01" layer="51"/>
<wire x1="-9.75" y1="8.6505" x2="-7.95" y2="8.6505" width="0.01" layer="51"/>
<wire x1="-9.75" y1="9.0747" x2="-7.95" y2="9.0747" width="0.01" layer="51"/>
<wire x1="-9.75" y1="9.2601" x2="-7.95" y2="9.2601" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-9.2601" x2="-7.95" y2="-9.2601" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-9.0747" x2="-7.95" y2="-9.0747" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-8.6505" x2="-7.95" y2="-8.6505" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-8.4651" x2="-7.95" y2="-8.4651" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-6.5349" x2="-7.95" y2="-6.5349" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-6.3495" x2="-7.95" y2="-6.3495" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-5.9253" x2="-7.95" y2="-5.9253" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-5.7399" x2="-7.95" y2="-5.7399" width="0.01" layer="51"/>
<wire x1="-9.75" y1="0.7399" x2="-7.95" y2="0.7399" width="0.01" layer="51"/>
<wire x1="-9.75" y1="0.9253" x2="-7.95" y2="0.9253" width="0.01" layer="51"/>
<wire x1="-9.75" y1="1.3495" x2="-7.95" y2="1.3495" width="0.01" layer="51"/>
<wire x1="-9.75" y1="1.5349" x2="-7.95" y2="1.5349" width="0.01" layer="51"/>
<wire x1="-9.75" y1="3.4651" x2="-7.95" y2="3.4651" width="0.01" layer="51"/>
<wire x1="-9.75" y1="3.6505" x2="-7.95" y2="3.6505" width="0.01" layer="51"/>
<wire x1="-9.75" y1="4.0747" x2="-7.95" y2="4.0747" width="0.01" layer="51"/>
<wire x1="-9.75" y1="4.2601" x2="-7.95" y2="4.2601" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-4.2601" x2="-7.95" y2="-4.2601" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-4.0747" x2="-7.95" y2="-4.0747" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-3.6505" x2="-7.95" y2="-3.6505" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-3.4651" x2="-7.95" y2="-3.4651" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-1.5349" x2="-7.95" y2="-1.5349" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-1.3495" x2="-7.95" y2="-1.3495" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-0.9253" x2="-7.95" y2="-0.9253" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-0.7399" x2="-7.95" y2="-0.7399" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="-8.2357" x2="-9.1892" y2="-10" width="0.01" layer="51"/>
<wire x1="-8.35" y1="-8.1" x2="-8.35" y2="-10" width="0.01" layer="51"/>
<wire x1="-9.35" y1="10" x2="-9.35" y2="8.4354" width="0.01" layer="51"/>
<wire x1="-9.35" y1="6.5646" x2="-9.35" y2="5.5" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="10" x2="-9.1892" y2="8.2357" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="6.7643" x2="-9.1892" y2="5.5" width="0.01" layer="51"/>
<wire x1="-8.35" y1="10" x2="-8.35" y2="8.1" width="0.01" layer="51"/>
<wire x1="-8.35" y1="6.9" x2="-8.35" y2="5.5" width="0.01" layer="51"/>
<wire x1="-8.85" y1="6.9" x2="-9.2243" y2="6.7503" width="0.01" layer="51"/>
<wire x1="-7.95" y1="6.9" x2="-8.85" y2="6.9" width="0.01" layer="51"/>
<wire x1="-8.85" y1="8.1" x2="-7.95" y2="8.1" width="0.01" layer="51"/>
<wire x1="-9.2243" y1="8.2497" x2="-8.85" y2="8.1" width="0.01" layer="51"/>
<wire x1="-9.35" y1="-8.4354" x2="-9.35" y2="-10" width="0.01" layer="51"/>
<wire x1="-9.35" y1="-5.5" x2="-9.35" y2="-6.5646" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="-5.5" x2="-9.1892" y2="-6.7643" width="0.01" layer="51"/>
<wire x1="-8.35" y1="-5.5" x2="-8.35" y2="-6.9" width="0.01" layer="51"/>
<wire x1="-9.2243" y1="-6.7503" x2="-8.85" y2="-6.9" width="0.01" layer="51"/>
<wire x1="-8.85" y1="-6.9" x2="-7.95" y2="-6.9" width="0.01" layer="51"/>
<wire x1="-7.95" y1="-8.1" x2="-8.85" y2="-8.1" width="0.01" layer="51"/>
<wire x1="-8.85" y1="-8.1" x2="-9.2243" y2="-8.2497" width="0.01" layer="51"/>
<wire x1="-9.35" y1="-0.5" x2="-9.35" y2="-1.5646" width="0.01" layer="51"/>
<wire x1="-9.35" y1="-3.4354" x2="-9.35" y2="-4.5" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="-0.5" x2="-9.1892" y2="-1.7643" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="-3.2357" x2="-9.1892" y2="-4.5" width="0.01" layer="51"/>
<wire x1="-8.35" y1="-0.5" x2="-8.35" y2="-1.9" width="0.01" layer="51"/>
<wire x1="-8.35" y1="-3.1" x2="-8.35" y2="-4.5" width="0.01" layer="51"/>
<wire x1="-8.85" y1="-3.1" x2="-9.2243" y2="-3.2497" width="0.01" layer="51"/>
<wire x1="-7.95" y1="-3.1" x2="-8.85" y2="-3.1" width="0.01" layer="51"/>
<wire x1="-8.85" y1="-1.9" x2="-7.95" y2="-1.9" width="0.01" layer="51"/>
<wire x1="-9.2243" y1="-1.7503" x2="-8.85" y2="-1.9" width="0.01" layer="51"/>
<wire x1="-9.35" y1="4.5" x2="-9.35" y2="3.4354" width="0.01" layer="51"/>
<wire x1="-9.35" y1="1.5646" x2="-9.35" y2="0.5" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="4.5" x2="-9.1892" y2="3.2357" width="0.01" layer="51"/>
<wire x1="-9.1892" y1="1.7643" x2="-9.1892" y2="0.5" width="0.01" layer="51"/>
<wire x1="-8.35" y1="4.5" x2="-8.35" y2="3.1" width="0.01" layer="51"/>
<wire x1="-8.35" y1="1.9" x2="-8.35" y2="0.5" width="0.01" layer="51"/>
<wire x1="-8.85" y1="1.9" x2="-9.2243" y2="1.7503" width="0.01" layer="51"/>
<wire x1="-7.95" y1="1.9" x2="-8.85" y2="1.9" width="0.01" layer="51"/>
<wire x1="-8.85" y1="3.1" x2="-7.95" y2="3.1" width="0.01" layer="51"/>
<wire x1="-9.2243" y1="3.2497" x2="-8.85" y2="3.1" width="0.01" layer="51"/>
<wire x1="-9.75" y1="5.5" x2="-9.35" y2="5.5" width="0.01" layer="51"/>
<wire x1="-3.35" y1="4.6" x2="-3.35" y2="5.4" width="0.01" layer="51"/>
<wire x1="-9.35" y1="4.5" x2="-9.75" y2="4.5" width="0.01" layer="51"/>
<wire x1="-9.75" y1="10" x2="-9.35" y2="10" width="0.01" layer="51"/>
<wire x1="5.65" y1="11.1" x2="5.65" y2="9.05" width="0.01" layer="51"/>
<wire x1="5.65" y1="9.05" x2="5.65" y2="8.25" width="0.01" layer="51"/>
<wire x1="5.65" y1="8.25" x2="5.65" y2="6.75" width="0.01" layer="51"/>
<wire x1="5.65" y1="6.75" x2="5.65" y2="5.4" width="0.01" layer="51"/>
<wire x1="-7.95" y1="5.4" x2="5.65" y2="5.4" width="0.01" layer="51"/>
<wire x1="-7.95" y1="10" x2="-7.95" y2="5.4" width="0.01" layer="51"/>
<wire x1="-9.35" y1="-10" x2="-9.75" y2="-10" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-0.5" x2="-9.35" y2="-0.5" width="0.01" layer="51"/>
<wire x1="-9.35" y1="0.5" x2="-9.75" y2="0.5" width="0.01" layer="51"/>
<wire x1="-7.95" y1="4.6" x2="-7.95" y2="0.5" width="0.01" layer="51"/>
<wire x1="5.65" y1="4.6" x2="-7.95" y2="4.6" width="0.01" layer="51"/>
<wire x1="5.65" y1="-5.4" x2="5.65" y2="-6.75" width="0.01" layer="51"/>
<wire x1="5.65" y1="-6.75" x2="5.65" y2="-8.25" width="0.01" layer="51"/>
<wire x1="5.65" y1="-8.25" x2="5.65" y2="-9.05" width="0.01" layer="51"/>
<wire x1="5.65" y1="-9.05" x2="5.65" y2="-11.1" width="0.01" layer="51"/>
<wire x1="5.65" y1="4.6" x2="5.65" y2="3.25" width="0.01" layer="51"/>
<wire x1="5.65" y1="3.25" x2="5.65" y2="1.75" width="0.01" layer="51"/>
<wire x1="5.65" y1="1.75" x2="5.65" y2="-1.75" width="0.01" layer="51"/>
<wire x1="5.65" y1="-1.75" x2="5.65" y2="-3.25" width="0.01" layer="51"/>
<wire x1="5.65" y1="-3.25" x2="5.65" y2="-4.6" width="0.01" layer="51"/>
<wire x1="5.65" y1="-5.4" x2="-3.35" y2="-5.4" width="0.01" layer="51"/>
<wire x1="-3.35" y1="-5.4" x2="-7.95" y2="-5.4" width="0.01" layer="51"/>
<wire x1="-7.95" y1="-4.6" x2="-3.35" y2="-4.6" width="0.01" layer="51"/>
<wire x1="-3.35" y1="-4.6" x2="5.65" y2="-4.6" width="0.01" layer="51"/>
<wire x1="-3.35" y1="-5.4" x2="-3.35" y2="-4.6" width="0.01" layer="51"/>
<wire x1="-7.95" y1="-0.5" x2="-7.95" y2="-4.5" width="0.01" layer="51"/>
<wire x1="-7.95" y1="-4.5" x2="-7.95" y2="-4.6" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-4.5" x2="-9.35" y2="-4.5" width="0.01" layer="51"/>
<wire x1="-9.75" y1="11.1" x2="-9.75" y2="-5.5" width="0.01" layer="51"/>
<wire x1="-9.75" y1="-5.5" x2="-9.75" y2="-11.1" width="0.01" layer="51"/>
<wire x1="-9.35" y1="-5.5" x2="-9.75" y2="-5.5" width="0.01" layer="51"/>
<wire x1="-7.95" y1="-5.4" x2="-7.95" y2="-10" width="0.01" layer="51"/>
<wire x1="8.15" y1="-9.05" x2="5.65" y2="-9.05" width="0.01" layer="51"/>
<wire x1="9.75" y1="9.05" x2="8.15" y2="9.05" width="0.2" layer="21"/>
<wire x1="6.4" y1="1.75" x2="5.65" y2="1.75" width="0.01" layer="51"/>
<wire x1="6.4" y1="3.25" x2="6.4" y2="1.75" width="0.01" layer="51"/>
<wire x1="5.65" y1="3.25" x2="6.4" y2="3.25" width="0.01" layer="51"/>
<wire x1="6.4" y1="-3.25" x2="5.65" y2="-3.25" width="0.01" layer="51"/>
<wire x1="6.4" y1="-1.75" x2="6.4" y2="-3.25" width="0.01" layer="51"/>
<wire x1="5.65" y1="-1.75" x2="6.4" y2="-1.75" width="0.01" layer="51"/>
<wire x1="6.4" y1="-8.25" x2="5.65" y2="-8.25" width="0.01" layer="51"/>
<wire x1="6.4" y1="-6.75" x2="6.4" y2="-8.25" width="0.01" layer="51"/>
<wire x1="5.65" y1="-6.75" x2="6.4" y2="-6.75" width="0.01" layer="51"/>
<wire x1="6.4" y1="6.75" x2="5.65" y2="6.75" width="0.01" layer="51"/>
<wire x1="6.4" y1="8.25" x2="6.4" y2="6.75" width="0.01" layer="51"/>
<wire x1="5.65" y1="8.25" x2="6.4" y2="8.25" width="0.01" layer="51"/>
<wire x1="9.75" y1="9.05" x2="9.75" y2="-9.05" width="0.2" layer="21"/>
<wire x1="-9.75" y1="11.1" x2="-9.75" y2="8.65" width="0.2" layer="21"/>
<wire x1="-9.75" y1="6.35" x2="-9.75" y2="3.65" width="0.2" layer="21"/>
<wire x1="-9.75" y1="1.35" x2="-9.75" y2="-1.35" width="0.2" layer="21"/>
<wire x1="-9.75" y1="-3.65" x2="-9.75" y2="-6.35" width="0.2" layer="21"/>
<wire x1="-9.75" y1="-8.65" x2="-9.75" y2="-11.1" width="0.2" layer="21"/>
<circle x="-8.55" y="7.5" radius="0.25" width="0.01" layer="51"/>
<circle x="-8.55" y="7.5" radius="0.7" width="0.01" layer="51"/>
<circle x="-8.55" y="-7.5" radius="0.25" width="0.01" layer="51"/>
<circle x="-8.55" y="-7.5" radius="0.7" width="0.01" layer="51"/>
<circle x="-8.55" y="-2.5" radius="0.25" width="0.01" layer="51"/>
<circle x="-8.55" y="-2.5" radius="0.7" width="0.01" layer="51"/>
<circle x="-8.55" y="2.5" radius="0.25" width="0.01" layer="51"/>
<circle x="-8.55" y="2.5" radius="0.7" width="0.01" layer="51"/>
<pad name="L4" x="-8.55" y="7.5" drill="1.6" diameter="2" shape="long"/>
<pad name="L3" x="-8.55" y="2.5" drill="1.6" diameter="2" shape="long"/>
<pad name="L2" x="-8.55" y="-2.5" drill="1.6" diameter="2" shape="long"/>
<pad name="L1" x="-8.55" y="-7.5" drill="1.6" diameter="2" shape="long"/>
<text x="-9.35" y="11.7" size="2" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-9.25" y="-13.9" size="2" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="-11.5" y="-7.5" size="2" layer="21" font="vector" ratio="10" rot="R90">1</text>
</package>
</packages>
<symbols>
<symbol name="4-POL-S">
<wire x1="3.81" y1="-10.16" x2="-3.81" y2="-10.16" width="0.254" layer="97"/>
<wire x1="-3.81" y1="-10.16" x2="-3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="-10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-3.81" y1="-5.08" x2="-3.81" y2="0" width="0.254" layer="97"/>
<wire x1="-3.81" y1="0" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="0" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="5.08" x2="-3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="0" width="0.254" layer="97"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="10.16" width="0.254" layer="97"/>
<wire x1="3.81" y1="10.16" x2="-3.81" y2="10.16" width="0.254" layer="97"/>
<wire x1="-3.81" y1="10.16" x2="-3.81" y2="5.08" width="0.254" layer="97"/>
<text x="-3.5" y="12.46" size="2" layer="95" ratio="10">&gt;NAME</text>
<text x="-3.53" y="-13.294" size="2" layer="96" ratio="10">&gt;VALUE</text>
<pin name="P1" x="-1.27" y="-7.62" visible="pad" length="short" function="dot"/>
<pin name="P2" x="-1.27" y="-2.54" visible="pad" length="short" function="dot"/>
<pin name="P3" x="-1.27" y="2.54" visible="pad" length="short" function="dot"/>
<pin name="P4" x="-1.27" y="7.62" visible="pad" length="short" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2092-1424" prefix="X">
<description>&lt;b&gt;Serie 2092,  Stiftleiste mit abgewinkelten Lötstiften 4-polig / Series 2092,  Male header with angled solder pins 4-pole&lt;/b&gt;&lt;br&gt;&lt;br&gt;Polzahl / Pole No.: 4 &lt;br&gt;Rastermaß / Pitch: 5  mm&lt;br&gt;Bemessungsspannung / Rated Voltage: 320 V&lt;br&gt;Nennstrom / Nominal Current: 16 A&lt;br&gt;Lötstiftlänge: / Solder Pin Length: 3.6 mm&lt;br&gt;Farbe / Color: lichtgrau / light gray&lt;br&gt;&lt;br&gt; Stand: 01.01.2014. Die jeweils aktuellen Daten zu diesem Artikel finden Sie in unserem Produktkatalog, den Sie unter &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt; einsehen können. &lt;br&gt; As of: 01/01/2014. Please find the newest data for this article in our product catalogue, which can be viewed under &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt;.&lt;br&gt;</description>
<gates>
<gate name="G$1" symbol="4-POL-S" x="0" y="0"/>
</gates>
<devices>
<device name="" package="P-2092-1424">
<connects>
<connect gate="G$1" pin="P1" pad="L1"/>
<connect gate="G$1" pin="P2" pad="L2"/>
<connect gate="G$1" pin="P3" pad="L3"/>
<connect gate="G$1" pin="P4" pad="L4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-wago_3.5mm">
<description>&lt;b&gt;WAGO picoMAX 3.5 - Serie 2091&lt;br&gt;
Rastermaß/ Pitch 3.5 mm
&lt;/b&gt;&lt;br&gt;

&lt;UL&gt;
&lt;LI&gt; Leiterquerschnitte bis 1,5 mm²&lt;br&gt;
Conductor size up to 1.5 mm²&lt;/LI&gt;

&lt;br&gt;


&lt;LI&gt; Gerade und abgewinkelte Lötstifte für rechtwinklige und parallele Steckrichtung zur Leiterplatte &lt;br&gt;
Horizontal or vertical PCB mounting via straight or angled solder pins &lt;/LI&gt;

&lt;br&gt;

&lt;LI&gt; Stift- und Federleiste mit Lötstiften&lt;br&gt;
Male and Femail Header with solder pins &lt;/LI&gt;

&lt;br&gt;

&lt;LI&gt; THR-Varianten zur Verarbeitung im Reflow-Lötprozess in der SMT-Fertigung &lt;br&gt;
THR versions for reflow soldering in SMT applications &lt;/LI&gt;

&lt;br&gt;

&lt;LI&gt; Tape-and-Reel-Verpackungen für die automatische Bestückung der Komponenten auf die Leiterplatte &lt;br&gt;
Available in tape-and-reel packaging for automated pick-and-place assembly&lt;/LI&gt;

&lt;br&gt;</description>
<packages>
<package name="P-2091-1422">
<wire x1="2.25" y1="2.25" x2="-4.55" y2="2.25" width="0.01" layer="51"/>
<wire x1="-4.55" y1="1.25" x2="2.25" y2="1.25" width="0.01" layer="51"/>
<wire x1="3.25" y1="1.95" x2="2.25" y2="2.25" width="0.01" layer="51"/>
<wire x1="2.25" y1="1.25" x2="3.25" y2="1.55" width="0.01" layer="51"/>
<wire x1="2.25" y1="-1.25" x2="-4.55" y2="-1.25" width="0.01" layer="51"/>
<wire x1="-4.55" y1="-2.25" x2="2.25" y2="-2.25" width="0.01" layer="51"/>
<wire x1="3.25" y1="-1.55" x2="2.25" y2="-1.25" width="0.01" layer="51"/>
<wire x1="2.25" y1="-2.25" x2="3.25" y2="-1.95" width="0.01" layer="51"/>
<wire x1="-8.75" y1="4.5" x2="6.75" y2="4.5" width="0.2" layer="21"/>
<wire x1="6.25" y1="4.5" x2="6.25" y2="3.7" width="0.01" layer="51"/>
<wire x1="6.25" y1="-3.7" x2="6.25" y2="-4.5" width="0.01" layer="51"/>
<wire x1="-6.784" y1="3.7" x2="-6.784" y2="-3.7" width="0.01" layer="51"/>
<wire x1="6.75" y1="3.7" x2="6.3794" y2="3.7" width="0.01" layer="51"/>
<wire x1="5.0206" y1="3.7" x2="4.65" y2="3.7" width="0.01" layer="51"/>
<wire x1="4.65" y1="3.7" x2="-2.35" y2="3.7" width="0.01" layer="51"/>
<wire x1="-2.35" y1="3.7" x2="-6.25" y2="3.7" width="0.01" layer="51"/>
<wire x1="-6.25" y1="3.7" x2="-6.65" y2="3.7" width="0.01" layer="51"/>
<wire x1="-6.65" y1="3.7" x2="-8.75" y2="3.7" width="0.01" layer="51"/>
<wire x1="1.6964" y1="2.8" x2="1.6964" y2="-2.8" width="0.01" layer="51"/>
<wire x1="1.2043" y1="2.8" x2="1.2043" y2="-2.8" width="0.01" layer="51"/>
<wire x1="0.6867" y1="2.8" x2="0.6867" y2="-2.8" width="0.01" layer="51"/>
<wire x1="-1.0867" y1="2.8" x2="-1.0867" y2="-2.8" width="0.01" layer="51"/>
<wire x1="-1.6043" y1="2.8" x2="-1.6043" y2="-2.8" width="0.01" layer="51"/>
<wire x1="-8.75" y1="-3.7" x2="-6.65" y2="-3.7" width="0.01" layer="51"/>
<wire x1="-6.65" y1="-3.7" x2="-6.25" y2="-3.7" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-3.7" x2="-2.35" y2="-3.7" width="0.01" layer="51"/>
<wire x1="-2.35" y1="-3.7" x2="5.0206" y2="-3.7" width="0.01" layer="51"/>
<wire x1="6.3794" y1="-3.7" x2="6.75" y2="-3.7" width="0.01" layer="51"/>
<wire x1="-6.65" y1="3.7" x2="-6.65" y2="-3.7" width="0.01" layer="51"/>
<wire x1="-2.0964" y1="2.8" x2="-2.0964" y2="-2.8" width="0.01" layer="51"/>
<wire x1="-2.35" y1="3.7" x2="-2.35" y2="2.8" width="0.01" layer="51"/>
<wire x1="-2.35" y1="-2.8" x2="-2.35" y2="-3.7" width="0.01" layer="51"/>
<wire x1="6.75" y1="-2.8" x2="1.75" y2="-2.8" width="0.01" layer="51"/>
<wire x1="1.75" y1="-2.8" x2="-4.55" y2="-2.8" width="0.01" layer="51"/>
<wire x1="-8.45" y1="-2.9395" x2="-8.2985" y2="-2.9342" width="0.01" layer="51"/>
<wire x1="-8.2985" y1="-2.9342" x2="-7.65" y2="-2.9116" width="0.01" layer="51"/>
<wire x1="-7.65" y1="-2.9116" x2="-6.95" y2="-2.8871" width="0.01" layer="51"/>
<wire x1="-8.45" y1="0.5605" x2="-8.2985" y2="0.5658" width="0.01" layer="51"/>
<wire x1="-8.2985" y1="0.5658" x2="-7.65" y2="0.5884" width="0.01" layer="51"/>
<wire x1="-7.65" y1="0.5884" x2="-6.95" y2="0.6129" width="0.01" layer="51"/>
<wire x1="-6.95" y1="-0.6129" x2="-8.2985" y2="-0.5658" width="0.01" layer="51"/>
<wire x1="-8.2985" y1="-0.5658" x2="-8.45" y2="-0.5605" width="0.01" layer="51"/>
<wire x1="3.6" y1="1.2" x2="3.3899" y2="1.2" width="0.01" layer="51"/>
<wire x1="3.3899" y1="1.2" x2="3.15" y2="1.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="-2.3" x2="3.3899" y2="-2.3" width="0.01" layer="51"/>
<wire x1="3.3899" y1="-2.3" x2="1.75" y2="-2.3" width="0.01" layer="51"/>
<wire x1="1.75" y1="-0.975" x2="2.2502" y2="-1.0554" width="0.01" layer="51"/>
<wire x1="2.2502" y1="-1.0554" x2="3.15" y2="-1.2" width="0.01" layer="51"/>
<wire x1="1.75" y1="2.3" x2="3.3899" y2="2.3" width="0.01" layer="51"/>
<wire x1="3.3899" y1="2.3" x2="3.6" y2="2.3" width="0.01" layer="51"/>
<wire x1="3.15" y1="-1.2" x2="3.3899" y2="-1.2" width="0.01" layer="51"/>
<wire x1="3.3899" y1="-1.2" x2="3.6" y2="-1.2" width="0.01" layer="51"/>
<wire x1="1.75" y1="-2.3" x2="1.75" y2="-2.8" width="0.01" layer="51"/>
<wire x1="3.3899" y1="-2.3" x2="3.3899" y2="-1.2" width="0.01" layer="51"/>
<wire x1="3.6" y1="-1.2" x2="3.6" y2="-2.3" width="0.01" layer="51"/>
<wire x1="3.6" y1="2.3" x2="3.6" y2="1.2" width="0.01" layer="51"/>
<wire x1="3.75" y1="2.276" x2="3.75" y2="1.224" width="0.01" layer="51"/>
<wire x1="4.65" y1="2.276" x2="3.75" y2="2.276" width="0.01" layer="51"/>
<wire x1="3.75" y1="1.224" x2="4.65" y2="1.224" width="0.01" layer="51"/>
<wire x1="3.75" y1="-1.224" x2="3.75" y2="-2.276" width="0.01" layer="51"/>
<wire x1="4.65" y1="-1.224" x2="3.75" y2="-1.224" width="0.01" layer="51"/>
<wire x1="3.75" y1="-2.276" x2="4.65" y2="-2.276" width="0.01" layer="51"/>
<wire x1="6.75" y1="2.8" x2="4.65" y2="2.8" width="0.01" layer="51"/>
<wire x1="4.65" y1="2.8" x2="1.75" y2="2.8" width="0.01" layer="51"/>
<wire x1="1.75" y1="2.8" x2="-4.55" y2="2.8" width="0.01" layer="51"/>
<wire x1="-6.25" y1="2.95" x2="-4.55" y2="2.95" width="0.01" layer="51"/>
<wire x1="-4.55" y1="-2.95" x2="-4.55" y2="-1.1224" width="0.01" layer="51"/>
<wire x1="-4.55" y1="-1.1224" x2="-4.55" y2="-1.1156" width="0.01" layer="51"/>
<wire x1="-4.55" y1="-1.1156" x2="-4.55" y2="-1.0638" width="0.01" layer="51"/>
<wire x1="-4.55" y1="-1.0638" x2="-4.55" y2="-1.0554" width="0.01" layer="51"/>
<wire x1="-4.55" y1="-1.0554" x2="-4.55" y2="-0.881" width="0.01" layer="51"/>
<wire x1="-4.55" y1="-0.881" x2="-4.55" y2="-0.7395" width="0.01" layer="51"/>
<wire x1="-4.55" y1="-0.7395" x2="-4.55" y2="-0.55" width="0.01" layer="51"/>
<wire x1="-4.55" y1="-2.95" x2="-6.25" y2="-2.95" width="0.01" layer="51"/>
<wire x1="-6.25" y1="3.7" x2="-6.25" y2="1.1224" width="0.01" layer="51"/>
<wire x1="-6.25" y1="1.1224" x2="-6.25" y2="1.1156" width="0.01" layer="51"/>
<wire x1="-6.25" y1="1.1156" x2="-6.25" y2="1.0638" width="0.01" layer="51"/>
<wire x1="-6.25" y1="1.0638" x2="-6.25" y2="0.975" width="0.01" layer="51"/>
<wire x1="-6.25" y1="0.975" x2="-6.25" y2="0.881" width="0.01" layer="51"/>
<wire x1="-6.25" y1="0.881" x2="-6.25" y2="0.7395" width="0.01" layer="51"/>
<wire x1="-6.25" y1="0.7395" x2="-6.25" y2="0.55" width="0.01" layer="51"/>
<wire x1="-6.25" y1="0.55" x2="-6.25" y2="-0.55" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-0.55" x2="-6.25" y2="-0.7395" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-0.7395" x2="-6.25" y2="-0.881" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-0.881" x2="-6.25" y2="-0.975" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-0.975" x2="-6.25" y2="-1.0638" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-1.0638" x2="-6.25" y2="-1.1156" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-1.1156" x2="-6.25" y2="-1.1224" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-1.1224" x2="-6.25" y2="-3.7" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-0.55" x2="-5.25" y2="-0.55" width="0.01" layer="51"/>
<wire x1="-5.25" y1="-0.55" x2="-4.55" y2="-0.55" width="0.01" layer="51"/>
<wire x1="3.15" y1="1.2" x2="2.2502" y2="1.0554" width="0.01" layer="51"/>
<wire x1="2.2502" y1="1.0554" x2="1.75" y2="0.975" width="0.01" layer="51"/>
<wire x1="1.75" y1="2.8" x2="1.75" y2="2.3" width="0.01" layer="51"/>
<wire x1="3.3899" y1="1.2" x2="3.3899" y2="2.3" width="0.01" layer="51"/>
<wire x1="-4.55" y1="0.55" x2="-4.55" y2="0.7395" width="0.01" layer="51"/>
<wire x1="-4.55" y1="0.7395" x2="-4.55" y2="0.881" width="0.01" layer="51"/>
<wire x1="-4.55" y1="0.881" x2="-4.55" y2="1.0554" width="0.01" layer="51"/>
<wire x1="-4.55" y1="1.0554" x2="-4.55" y2="1.0638" width="0.01" layer="51"/>
<wire x1="-4.55" y1="1.0638" x2="-4.55" y2="1.1156" width="0.01" layer="51"/>
<wire x1="-4.55" y1="1.1156" x2="-4.55" y2="1.1224" width="0.01" layer="51"/>
<wire x1="-4.55" y1="1.1224" x2="-4.55" y2="2.95" width="0.01" layer="51"/>
<wire x1="-6.25" y1="0.55" x2="-5.25" y2="0.55" width="0.01" layer="51"/>
<wire x1="-5.25" y1="0.55" x2="-4.55" y2="0.55" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-1.1156" x2="-4.55" y2="-1.1156" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-0.975" x2="1.75" y2="-0.975" width="0.01" layer="51"/>
<wire x1="2.2502" y1="-1.0554" x2="-4.55" y2="-1.0554" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-0.881" x2="-4.55" y2="-0.881" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-1.0638" x2="-4.55" y2="-1.0638" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-1.1224" x2="-4.55" y2="-1.1224" width="0.01" layer="51"/>
<wire x1="-4.55" y1="1.0554" x2="2.2502" y2="1.0554" width="0.01" layer="51"/>
<wire x1="1.75" y1="0.975" x2="-6.25" y2="0.975" width="0.01" layer="51"/>
<wire x1="-6.25" y1="1.1156" x2="-4.55" y2="1.1156" width="0.01" layer="51"/>
<wire x1="-6.25" y1="1.1224" x2="-4.55" y2="1.1224" width="0.01" layer="51"/>
<wire x1="-6.25" y1="1.0638" x2="-4.55" y2="1.0638" width="0.01" layer="51"/>
<wire x1="-6.25" y1="0.881" x2="-4.55" y2="0.881" width="0.01" layer="51"/>
<wire x1="-5.25" y1="-0.55" x2="-5.25" y2="0.55" width="0.01" layer="51"/>
<wire x1="-6.25" y1="0.7395" x2="-4.55" y2="0.7395" width="0.01" layer="51"/>
<wire x1="-6.25" y1="-0.7395" x2="-4.55" y2="-0.7395" width="0.01" layer="51"/>
<wire x1="2.25" y1="1.25" x2="2.25" y2="2.25" width="0.01" layer="51"/>
<wire x1="3.25" y1="1.55" x2="3.25" y2="1.95" width="0.01" layer="51"/>
<wire x1="2.25" y1="-2.25" x2="2.25" y2="-1.25" width="0.01" layer="51"/>
<wire x1="3.25" y1="-1.95" x2="3.25" y2="-1.55" width="0.01" layer="51"/>
<wire x1="-8.475" y1="1.75" x2="-7.425" y2="1.75" width="0.01" layer="51"/>
<wire x1="-7.95" y1="1.225" x2="-7.95" y2="2.275" width="0.01" layer="51"/>
<wire x1="-8.475" y1="-1.75" x2="-7.425" y2="-1.75" width="0.01" layer="51"/>
<wire x1="-7.95" y1="-2.275" x2="-7.95" y2="-1.225" width="0.01" layer="51"/>
<wire x1="-4.94" y1="1.75" x2="3.64" y2="1.75" width="0.01" layer="51"/>
<wire x1="-4.94" y1="-1.75" x2="3.64" y2="-1.75" width="0.01" layer="51"/>
<wire x1="4.65" y1="4.5" x2="4.65" y2="3.7" width="0.01" layer="51"/>
<wire x1="4.65" y1="3.7" x2="4.65" y2="2.8" width="0.01" layer="51"/>
<wire x1="4.65" y1="2.8" x2="4.65" y2="2.276" width="0.01" layer="51"/>
<wire x1="4.65" y1="2.276" x2="4.65" y2="1.224" width="0.01" layer="51"/>
<wire x1="4.65" y1="1.224" x2="4.65" y2="0.4" width="0.01" layer="51"/>
<wire x1="5.0206" y1="-3.7" x2="5.0206" y2="-4.5" width="0.01" layer="51"/>
<wire x1="5.0206" y1="4.5" x2="5.0206" y2="3.7" width="0.01" layer="51"/>
<wire x1="6.75" y1="-4.5" x2="-8.75" y2="-4.5" width="0.2" layer="21"/>
<wire x1="6.3794" y1="-3.7" x2="6.3794" y2="-4.5" width="0.01" layer="51"/>
<wire x1="6.3794" y1="4.5" x2="6.3794" y2="3.7" width="0.01" layer="51"/>
<wire x1="6.75" y1="-2.8" x2="6.75" y2="-4.5" width="0.2" layer="21"/>
<wire x1="6.75" y1="4.5" x2="6.75" y2="2.8" width="0.2" layer="21"/>
<wire x1="5.0206" y1="3.7" x2="6.3794" y2="3.7" width="0.01" layer="51"/>
<wire x1="6.3794" y1="-3.7" x2="5.0206" y2="-3.7" width="0.01" layer="51"/>
<wire x1="8.75" y1="-2.8" x2="6.75" y2="-2.8" width="0.2" layer="21"/>
<wire x1="-8.75" y1="2.95" x2="-8.45" y2="2.9395" width="0.01" layer="51"/>
<wire x1="-8.45" y1="2.9395" x2="-8.2985" y2="2.9342" width="0.01" layer="51"/>
<wire x1="-8.2985" y1="2.9342" x2="-6.95" y2="2.8871" width="0.01" layer="51"/>
<wire x1="-7.65" y1="-2.2" x2="-7.65" y2="-2.9116" width="0.01" layer="51"/>
<wire x1="-8.45" y1="1.1268" x2="-8.45" y2="0.5605" width="0.01" layer="51"/>
<wire x1="-8.45" y1="2.9395" x2="-8.45" y2="2.3732" width="0.01" layer="51"/>
<wire x1="-7.65" y1="1.3" x2="-7.65" y2="0.5884" width="0.01" layer="51"/>
<wire x1="-8.2985" y1="1.2143" x2="-8.2985" y2="0.5658" width="0.01" layer="51"/>
<wire x1="-8.2985" y1="2.9342" x2="-8.2985" y2="2.2857" width="0.01" layer="51"/>
<wire x1="-7.65" y1="2.9116" x2="-7.65" y2="2.2" width="0.01" layer="51"/>
<wire x1="-6.95" y1="2.8871" x2="-6.95" y2="2.2" width="0.01" layer="51"/>
<wire x1="-6.95" y1="2.2" x2="-6.95" y2="1.3" width="0.01" layer="51"/>
<wire x1="-6.95" y1="1.3" x2="-6.95" y2="0.6129" width="0.01" layer="51"/>
<wire x1="-6.95" y1="0.6129" x2="-6.95" y2="0.4" width="0.01" layer="51"/>
<wire x1="-8.45" y1="2.3732" x2="-8.15" y2="2.2" width="0.01" layer="51"/>
<wire x1="-8.15" y1="2.2" x2="-6.95" y2="2.2" width="0.01" layer="51"/>
<wire x1="-8.15" y1="1.3" x2="-8.45" y2="1.1268" width="0.01" layer="51"/>
<wire x1="-6.95" y1="1.3" x2="-8.15" y2="1.3" width="0.01" layer="51"/>
<wire x1="-8.2985" y1="-2.2857" x2="-8.2985" y2="-2.9342" width="0.01" layer="51"/>
<wire x1="-8.45" y1="-2.3732" x2="-8.45" y2="-2.9395" width="0.01" layer="51"/>
<wire x1="-8.45" y1="-0.5605" x2="-8.45" y2="-1.1268" width="0.01" layer="51"/>
<wire x1="-8.2985" y1="-0.5658" x2="-8.2985" y2="-1.2143" width="0.01" layer="51"/>
<wire x1="-7.65" y1="-0.5884" x2="-7.65" y2="-1.3" width="0.01" layer="51"/>
<wire x1="-8.15" y1="-1.3" x2="-6.95" y2="-1.3" width="0.01" layer="51"/>
<wire x1="-6.95" y1="-2.2" x2="-8.15" y2="-2.2" width="0.01" layer="51"/>
<wire x1="-8.15" y1="-2.2" x2="-8.45" y2="-2.3732" width="0.01" layer="51"/>
<wire x1="-8.45" y1="-1.1268" x2="-8.15" y2="-1.3" width="0.01" layer="51"/>
<wire x1="-6.95" y1="-2.8871" x2="-8.75" y2="-2.95" width="0.01" layer="51"/>
<wire x1="4.65" y1="-0.4" x2="4.65" y2="-1.224" width="0.01" layer="51"/>
<wire x1="4.65" y1="-1.224" x2="4.65" y2="-2.276" width="0.01" layer="51"/>
<wire x1="4.65" y1="-2.276" x2="4.65" y2="-4.5" width="0.01" layer="51"/>
<wire x1="4.65" y1="-0.4" x2="-2.35" y2="-0.4" width="0.01" layer="51"/>
<wire x1="-2.35" y1="-0.4" x2="-6.95" y2="-0.4" width="0.01" layer="51"/>
<wire x1="-6.95" y1="0.4" x2="-2.35" y2="0.4" width="0.01" layer="51"/>
<wire x1="-2.35" y1="0.4" x2="4.65" y2="0.4" width="0.01" layer="51"/>
<wire x1="-2.35" y1="-0.4" x2="-2.35" y2="0.4" width="0.01" layer="51"/>
<wire x1="-8.75" y1="0.55" x2="-6.95" y2="0.6129" width="0.01" layer="51"/>
<wire x1="-8.75" y1="-4.5" x2="-8.75" y2="-0.55" width="0.01" layer="51"/>
<wire x1="-8.75" y1="-0.55" x2="-8.75" y2="4.5" width="0.01" layer="51"/>
<wire x1="-6.95" y1="-0.6129" x2="-8.75" y2="-0.55" width="0.01" layer="51"/>
<wire x1="-6.95" y1="-0.4" x2="-6.95" y2="-2.8871" width="0.01" layer="51"/>
<wire x1="8.75" y1="2.8" x2="6.75" y2="2.8" width="0.2" layer="21"/>
<wire x1="8.75" y1="2.8" x2="8.75" y2="-2.8" width="0.2" layer="21"/>
<wire x1="4.65" y1="2.276" x2="5.55" y2="2.276" width="0.01" layer="51"/>
<wire x1="5.55" y1="2.276" x2="5.55" y2="1.224" width="0.01" layer="51"/>
<wire x1="5.55" y1="1.224" x2="4.65" y2="1.224" width="0.01" layer="51"/>
<wire x1="4.65" y1="-1.224" x2="5.55" y2="-1.224" width="0.01" layer="51"/>
<wire x1="5.55" y1="-1.224" x2="5.55" y2="-2.276" width="0.01" layer="51"/>
<wire x1="5.55" y1="-2.276" x2="4.65" y2="-2.276" width="0.01" layer="51"/>
<wire x1="-8.75" y1="4.5" x2="-8.75" y2="2.95" width="0.2" layer="21"/>
<wire x1="-8.75" y1="-0.55" x2="-8.75" y2="0.55" width="0.2" layer="21"/>
<wire x1="-8.75" y1="-2.95" x2="-8.75" y2="-4.5" width="0.2" layer="21"/>
<circle x="-7.95" y="1.75" radius="0.2" width="0.01" layer="51"/>
<circle x="-7.95" y="1.75" radius="0.5" width="0.01" layer="51"/>
<circle x="-7.95" y="-1.75" radius="0.2" width="0.01" layer="51"/>
<circle x="-7.95" y="-1.75" radius="0.5" width="0.01" layer="51"/>
<pad name="L1" x="-7.95" y="-1.75" drill="1.2" shape="long"/>
<pad name="L2" x="-7.95" y="1.75" drill="1.2" shape="long"/>
<text x="-8.45" y="6.5" size="2" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-8.45" y="-7.9" size="2" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<text x="-10.72" y="-1.8" size="2" layer="21" font="vector" ratio="10" rot="R90">1</text>
</package>
</packages>
<symbols>
<symbol name="2-POL-S">
<wire x1="3.81" y1="-5.08" x2="-3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="-3.81" y1="-5.08" x2="-3.81" y2="0" width="0.254" layer="97"/>
<wire x1="-3.81" y1="0" x2="3.81" y2="0" width="0.254" layer="97"/>
<wire x1="3.81" y1="0" x2="3.81" y2="-5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="0" x2="3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="3.81" y1="5.08" x2="-3.81" y2="5.08" width="0.254" layer="97"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="0" width="0.254" layer="97"/>
<text x="-3.5" y="7" size="2" layer="95" ratio="10">&gt;NAME</text>
<text x="-3.5" y="-8.5" size="2" layer="96" ratio="10">&gt;VALUE</text>
<pin name="P1" x="-1.27" y="-2.54" visible="pad" length="short" function="dot"/>
<pin name="P2" x="-1.27" y="2.54" visible="pad" length="short" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="2091-1422" prefix="X">
<description>&lt;b&gt;Serie 2091,  Stiftleiste mit abgewinkelten Lötstiften 2-polig / Series 2091,  Male header with angled solder pins 2-pole&lt;/b&gt;&lt;br&gt;&lt;br&gt;Polzahl / Pole No.: 2 &lt;br&gt;Rastermaß / Pitch: 3.5  mm&lt;br&gt;Bemessungsspannung / Rated Voltage: 160 V&lt;br&gt;Nennstrom / Nominal Current: 10 A&lt;br&gt;Lötstiftlänge: / Solder Pin Length: 3.6 mm&lt;br&gt;Farbe / Color: lichtgrau / light gray&lt;br&gt;&lt;br&gt; Stand: 01.01.2014. Die jeweils aktuellen Daten zu diesem Artikel finden Sie in unserem Produktkatalog, den Sie unter &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt; einsehen können. &lt;br&gt; As of: 01/01/2014. Please find the newest data for this article in our product catalogue, which can be viewed under &lt;a href="http://www.wago.com"&gt;www.wago.com&lt;/a&gt;.&lt;br&gt;</description>
<gates>
<gate name="G$1" symbol="2-POL-S" x="0" y="0"/>
</gates>
<devices>
<device name="" package="P-2091-1422">
<connects>
<connect gate="G$1" pin="P1" pad="L1"/>
<connect gate="G$1" pin="P2" pad="L2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-molex-c-grid">
<description>&lt;b&gt;MOLEX C-Grid Connectors&lt;/b&gt; - v1.02 (12/20/2009)&lt;p&gt;
Contains C-Grid and C-Grid III connectors&lt;p&gt;
NOTE: C-Grid III does not mate to C-Grid types
&lt;p&gt;THIS LIBRARY IS PROVIDED AS IS AND WITHOUT WARRANTY OF ANY KIND, EXPRESSED OR IMPLIED.&lt;br&gt;
USE AT YOUR OWN RISK!&lt;p&gt;
&lt;author&gt;Copyright (C) 2009, Bob Starr&lt;br&gt; http://www.bobstarr.net&lt;br&gt;&lt;/author&gt;</description>
<packages>
<package name="90136-1?03">
<description>&lt;b&gt;C-Grid III&lt;/b&gt;&lt;p&gt;
vertical</description>
<wire x1="5.3975" y1="-2.8575" x2="5.3975" y2="3.4925" width="0.254" layer="21"/>
<wire x1="5.3975" y1="3.4925" x2="4.445" y2="3.4925" width="0.254" layer="21"/>
<wire x1="4.445" y1="3.4925" x2="4.445" y2="2.8575" width="0.254" layer="21"/>
<wire x1="4.445" y1="2.8575" x2="4.1275" y2="2.54" width="0.254" layer="21" curve="-90.036103"/>
<wire x1="4.1275" y1="2.54" x2="-4.1275" y2="2.54" width="0.254" layer="21"/>
<wire x1="-4.1275" y1="2.54" x2="-4.445" y2="2.8575" width="0.254" layer="21" curve="-90.036103"/>
<wire x1="-4.445" y1="2.8575" x2="-4.445" y2="3.4925" width="0.254" layer="21"/>
<wire x1="-4.445" y1="3.4925" x2="-5.3975" y2="3.4925" width="0.254" layer="21"/>
<wire x1="-5.3975" y1="3.4925" x2="-5.3975" y2="-2.8575" width="0.254" layer="21"/>
<wire x1="-5.3975" y1="-2.8575" x2="-2.54" y2="-2.8575" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-2.8575" x2="-2.2225" y2="-3.81" width="0.254" layer="21"/>
<wire x1="-2.2225" y1="-3.81" x2="2.2225" y2="-3.81" width="0.254" layer="21"/>
<wire x1="2.2225" y1="-3.81" x2="2.54" y2="-2.8575" width="0.254" layer="21"/>
<wire x1="2.54" y1="-2.8575" x2="5.3975" y2="-2.8575" width="0.254" layer="21"/>
<wire x1="-4.7625" y1="1.5875" x2="-4.7625" y2="-2.2225" width="0.0508" layer="51"/>
<wire x1="4.7625" y1="1.5875" x2="4.7625" y2="-2.2225" width="0.0508" layer="51"/>
<wire x1="-4.7625" y1="1.5875" x2="4.7625" y2="1.5875" width="0.0508" layer="51"/>
<wire x1="-4.7625" y1="-2.2225" x2="-2.2225" y2="-2.2225" width="0.0508" layer="51"/>
<wire x1="-2.2225" y1="-2.2225" x2="2.2225" y2="-2.2225" width="0.0508" layer="51"/>
<wire x1="2.2225" y1="-2.2225" x2="4.7625" y2="-2.2225" width="0.0508" layer="51"/>
<wire x1="1.905" y1="-3.175" x2="-1.905" y2="-3.175" width="0.0508" layer="51"/>
<wire x1="-2.2225" y1="-2.2225" x2="-1.905" y2="-3.175" width="0.0508" layer="51"/>
<wire x1="2.2225" y1="-2.2225" x2="1.905" y2="-3.175" width="0.0508" layer="51"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.715" y="-2.54" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="-4.7625" y="3.81" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.1275" y="-1.5875" size="0.8128" layer="51" ratio="10">1</text>
<rectangle x1="-0.2381" y1="-0.2381" x2="0.2381" y2="0.2381" layer="51"/>
<rectangle x1="-2.7781" y1="-0.2381" x2="-2.3019" y2="0.2381" layer="51"/>
<rectangle x1="2.3019" y1="-0.2381" x2="2.7781" y2="0.2381" layer="51"/>
</package>
<package name="90136-1?02">
<description>&lt;b&gt;C-Grid III&lt;/b&gt;&lt;p&gt;
vertical</description>
<wire x1="4.1275" y1="-2.8575" x2="4.1275" y2="3.4925" width="0.254" layer="21"/>
<wire x1="4.1275" y1="3.4925" x2="3.175" y2="3.4925" width="0.254" layer="21"/>
<wire x1="3.175" y1="3.4925" x2="3.175" y2="2.8575" width="0.254" layer="21"/>
<wire x1="3.175" y1="2.8575" x2="2.8575" y2="2.54" width="0.254" layer="21" curve="-90.036103"/>
<wire x1="2.8575" y1="2.54" x2="-2.8575" y2="2.54" width="0.254" layer="21"/>
<wire x1="-2.8575" y1="2.54" x2="-3.175" y2="2.8575" width="0.254" layer="21" curve="-90.036103"/>
<wire x1="-3.175" y1="2.8575" x2="-3.175" y2="3.4925" width="0.254" layer="21"/>
<wire x1="-3.175" y1="3.4925" x2="-4.1275" y2="3.4925" width="0.254" layer="21"/>
<wire x1="-4.1275" y1="3.4925" x2="-4.1275" y2="-2.8575" width="0.254" layer="21"/>
<wire x1="-4.1275" y1="-2.8575" x2="-2.54" y2="-2.8575" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-2.8575" x2="-2.2225" y2="-3.81" width="0.254" layer="21"/>
<wire x1="-2.2225" y1="-3.81" x2="2.2225" y2="-3.81" width="0.254" layer="21"/>
<wire x1="2.2225" y1="-3.81" x2="2.54" y2="-2.8575" width="0.254" layer="21"/>
<wire x1="2.54" y1="-2.8575" x2="4.1275" y2="-2.8575" width="0.254" layer="21"/>
<wire x1="-3.4925" y1="1.5875" x2="-3.4925" y2="-2.2225" width="0.0508" layer="51"/>
<wire x1="3.4925" y1="1.5875" x2="3.4925" y2="-2.2225" width="0.0508" layer="51"/>
<wire x1="-3.4925" y1="1.5875" x2="3.4925" y2="1.5875" width="0.0508" layer="51"/>
<wire x1="-3.4925" y1="-2.2225" x2="-2.2225" y2="-2.2225" width="0.0508" layer="51"/>
<wire x1="-2.2225" y1="-2.2225" x2="2.2225" y2="-2.2225" width="0.0508" layer="51"/>
<wire x1="2.2225" y1="-2.2225" x2="3.4925" y2="-2.2225" width="0.0508" layer="51"/>
<wire x1="1.905" y1="-3.175" x2="-1.905" y2="-3.175" width="0.0508" layer="51"/>
<wire x1="-2.2225" y1="-2.2225" x2="-1.905" y2="-3.175" width="0.0508" layer="51"/>
<wire x1="2.2225" y1="-2.2225" x2="1.905" y2="-3.175" width="0.0508" layer="51"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-4.445" y="-2.54" size="1.016" layer="25" ratio="18" rot="R90">&gt;NAME</text>
<text x="-3.4925" y="3.81" size="0.8128" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.8575" y="-1.5875" size="0.8128" layer="51" ratio="10">1</text>
<rectangle x1="1.0319" y1="-0.2381" x2="1.5081" y2="0.2381" layer="51"/>
<rectangle x1="-1.5081" y1="-0.2381" x2="-1.0319" y2="0.2381" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MA03-1">
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<text x="-1.27" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-5.08" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="-5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="MA02-1">
<wire x1="-1.27" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<text x="-1.27" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="-5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="2" x="-5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="90136-1?03" prefix="J">
<description>&lt;b&gt;C-Grid III&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA03-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="90136-1?03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="90136-1?02" prefix="J">
<description>&lt;b&gt;C-Grid III&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA02-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="90136-1?02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="X1" library="con-wago_5.0mm" deviceset="2092-1424" device=""/>
<part name="X2" library="con-wago_5.0mm" deviceset="2092-1424" device=""/>
<part name="X3" library="con-wago_3.5mm" deviceset="2091-1422" device=""/>
<part name="J1" library="con-molex-c-grid" deviceset="90136-1?03" device=""/>
<part name="J2" library="con-molex-c-grid" deviceset="90136-1?02" device=""/>
<part name="J3" library="con-molex-c-grid" deviceset="90136-1?02" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-10.16" y="66.04" size="1.778" layer="91">32 VDC</text>
<text x="-10.16" y="60.96" size="1.778" layer="91">24 VDC IO</text>
<text x="-10.16" y="55.88" size="1.778" layer="91">24 VDC STO</text>
<text x="-10.16" y="50.8" size="1.778" layer="91">GND</text>
<text x="-10.16" y="71.12" size="1.778" layer="91">INPUT</text>
<text x="-10.16" y="33.02" size="1.778" layer="91">OUTPUT</text>
<text x="34.29" y="88.9" size="1.778" layer="91">MC_PWR</text>
<text x="57.15" y="88.9" size="1.778" layer="91">MC_STO</text>
<text x="80.01" y="88.9" size="1.778" layer="91">MC_IO</text>
<text x="100.33" y="88.9" size="1.778" layer="91">MC_AUX</text>
</plain>
<instances>
<instance part="X1" gate="G$1" x="10.16" y="58.42" rot="R180"/>
<instance part="X2" gate="G$1" x="10.16" y="25.4" rot="R180"/>
<instance part="X3" gate="G$1" x="40.64" y="78.74"/>
<instance part="J1" gate="G$1" x="60.96" y="78.74"/>
<instance part="J2" gate="G$1" x="83.82" y="78.74"/>
<instance part="J3" gate="G$1" x="104.14" y="78.74"/>
</instances>
<busses>
</busses>
<nets>
<net name="32VDC" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P1"/>
<wire x1="11.43" y1="66.04" x2="20.32" y2="66.04" width="0.1524" layer="91"/>
<wire x1="20.32" y1="66.04" x2="20.32" y2="33.02" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="P1"/>
<wire x1="20.32" y1="33.02" x2="11.43" y2="33.02" width="0.1524" layer="91"/>
<wire x1="20.32" y1="66.04" x2="34.29" y2="66.04" width="0.1524" layer="91"/>
<wire x1="34.29" y1="66.04" x2="34.29" y2="76.2" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="P1"/>
<wire x1="34.29" y1="76.2" x2="39.37" y2="76.2" width="0.1524" layer="91"/>
<junction x="20.32" y="66.04"/>
</segment>
</net>
<net name="24VDC_IO" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P2"/>
<wire x1="11.43" y1="60.96" x2="19.05" y2="60.96" width="0.1524" layer="91"/>
<wire x1="19.05" y1="60.96" x2="19.05" y2="27.94" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="P2"/>
<wire x1="19.05" y1="27.94" x2="11.43" y2="27.94" width="0.1524" layer="91"/>
<wire x1="19.05" y1="60.96" x2="73.66" y2="60.96" width="0.1524" layer="91"/>
<wire x1="73.66" y1="60.96" x2="93.98" y2="60.96" width="0.1524" layer="91"/>
<wire x1="93.98" y1="60.96" x2="93.98" y2="78.74" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="1"/>
<wire x1="93.98" y1="78.74" x2="99.06" y2="78.74" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="78.74" y1="78.74" x2="73.66" y2="78.74" width="0.1524" layer="91"/>
<wire x1="73.66" y1="78.74" x2="73.66" y2="60.96" width="0.1524" layer="91"/>
<junction x="73.66" y="60.96"/>
<junction x="19.05" y="60.96"/>
</segment>
</net>
<net name="24VDC_STO" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P3"/>
<wire x1="11.43" y1="55.88" x2="17.78" y2="55.88" width="0.1524" layer="91"/>
<wire x1="17.78" y1="55.88" x2="17.78" y2="22.86" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="P3"/>
<wire x1="17.78" y1="22.86" x2="11.43" y2="22.86" width="0.1524" layer="91"/>
<wire x1="17.78" y1="55.88" x2="52.07" y2="55.88" width="0.1524" layer="91"/>
<wire x1="52.07" y1="55.88" x2="52.07" y2="78.74" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="52.07" y1="78.74" x2="52.07" y2="81.28" width="0.1524" layer="91"/>
<wire x1="52.07" y1="81.28" x2="55.88" y2="81.28" width="0.1524" layer="91"/>
<pinref part="J1" gate="G$1" pin="2"/>
<wire x1="52.07" y1="78.74" x2="55.88" y2="78.74" width="0.1524" layer="91"/>
<junction x="17.78" y="55.88"/>
<junction x="52.07" y="78.74"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="P4"/>
<wire x1="11.43" y1="50.8" x2="16.51" y2="50.8" width="0.1524" layer="91"/>
<wire x1="16.51" y1="50.8" x2="16.51" y2="17.78" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="P4"/>
<wire x1="16.51" y1="17.78" x2="11.43" y2="17.78" width="0.1524" layer="91"/>
<wire x1="16.51" y1="50.8" x2="31.75" y2="50.8" width="0.1524" layer="91"/>
<wire x1="31.75" y1="50.8" x2="31.75" y2="81.28" width="0.1524" layer="91"/>
<pinref part="X3" gate="G$1" pin="P2"/>
<wire x1="31.75" y1="81.28" x2="39.37" y2="81.28" width="0.1524" layer="91"/>
<junction x="16.51" y="50.8"/>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="55.88" y1="76.2" x2="54.61" y2="76.2" width="0.1524" layer="91"/>
<wire x1="54.61" y1="76.2" x2="54.61" y2="50.8" width="0.1524" layer="91"/>
<wire x1="54.61" y1="50.8" x2="31.75" y2="50.8" width="0.1524" layer="91"/>
<junction x="31.75" y="50.8"/>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="78.74" y1="76.2" x2="76.2" y2="76.2" width="0.1524" layer="91"/>
<wire x1="76.2" y1="76.2" x2="76.2" y2="50.8" width="0.1524" layer="91"/>
<wire x1="76.2" y1="50.8" x2="54.61" y2="50.8" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="99.06" y1="76.2" x2="96.52" y2="76.2" width="0.1524" layer="91"/>
<wire x1="96.52" y1="76.2" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<wire x1="96.52" y1="50.8" x2="76.2" y2="50.8" width="0.1524" layer="91"/>
<junction x="54.61" y="50.8"/>
<junction x="76.2" y="50.8"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
